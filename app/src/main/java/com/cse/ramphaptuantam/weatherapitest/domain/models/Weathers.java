package com.cse.ramphaptuantam.weatherapitest.domain.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RamDieu on 2/16/2017.
 */

public class Weathers{
    @SerializedName("city")
    private City city;
    @SerializedName("list")
    private List<WeatherInfo> weatherInfo;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<WeatherInfo> getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(List<WeatherInfo> weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    public static class City{
        @SerializedName("name")
        private String name;
        @SerializedName("id")
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
