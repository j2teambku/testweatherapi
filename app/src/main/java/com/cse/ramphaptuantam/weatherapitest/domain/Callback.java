package com.cse.ramphaptuantam.weatherapitest.domain;

/**
 * Created by RamDieu on 2/16/2017.
 */

public interface Callback<T>{
    void callback(T result);
}
