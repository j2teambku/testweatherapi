package com.cse.ramphaptuantam.weatherapitest.domain;


import android.os.AsyncTask;
import android.util.Log;

import com.cse.ramphaptuantam.weatherapitest.domain.models.Weathers;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by RamDieu on 2/16/2017.
 */

public class WeatherApi {

    public static final String DEFAULT_LOCATION_ID = "1905480";
    public static final String API_KEY = "58a0cdf799b4780869d74022b3118d07";

    private static final Gson GSON = new Gson();

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/city";


    public static void getWeather(final Callback<Weathers> callback){
        new AsyncTask<Void, Void, Weathers>(){

            @Override
            protected Weathers doInBackground(Void ...params) {
                try {
                    Weathers weathers = getWeather();
                    return weathers;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Weathers weathers) {
                super.onPostExecute(weathers);
                if(callback != null){
                    callback.callback(weathers);
                }
            }
        }.execute();
    }


    public static Weathers getWeather() throws IOException{
        Weathers result = null;
        String url = BASE_URL + "?id=" + DEFAULT_LOCATION_ID + "&lang=vi&appid=" + API_KEY;

        try {
            HttpURLConnection conn = connect(url);
            if(conn.getResponseCode() == 200){
                String json = readResponse(conn.getInputStream());
                Log.d("Weather_JSON:", json+"");
                try {
                    result = parseJson(json);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                throw new IOException("Request failed -> response_code=" + conn.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        }

        return result;
    }

    private static Weathers parseJson(String json) throws Exception{
        Weathers weathers = GSON.fromJson(json, Weathers.class);
        return weathers;
    }



    private static HttpURLConnection connect(String url) throws IOException {
        URL u = new URL(url);
        return (HttpURLConnection) u.openConnection();
    }

    private static String readResponse(InputStream is) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] data = new byte[2048];
        int len = 0;
        while ((len = is.read(data, 0, data.length)) >= 0) {
            bos.write(data, 0, len);
        }
        return new String(bos.toByteArray(), "UTF-8");
    }

}
