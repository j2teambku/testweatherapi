package com.cse.ramphaptuantam.weatherapitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cse.ramphaptuantam.weatherapitest.domain.Callback;
import com.cse.ramphaptuantam.weatherapitest.domain.WeatherApi;
import com.cse.ramphaptuantam.weatherapitest.domain.models.WeatherInfo;
import com.cse.ramphaptuantam.weatherapitest.domain.models.Weathers;


public class MainActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
    }

    public void onClick(View v){
        WeatherApi.getWeather(new Callback<Weathers>() {
            @Override
            public void callback(Weathers result) {
                if(result != null){
                    if(result.getCity() == null){
                        Toast.makeText(getApplicationContext(), "Sorry! Server temporarily down.",
                                Toast.LENGTH_LONG).show();
                    }else{
                        String s = "City: " + result.getCity().getName() +"--Weather: " + result.getWeatherInfo()
                                .get(0).getWeather().get(0).getDescription()
                         + "--Time: " + result.getWeatherInfo().get(0).getDateTime()
                         + "--MinTemp: " + result.getWeatherInfo().get(0).getMain().getTempMin() +  "K"
                          + "--MaxTemp: " + result.getWeatherInfo().get(0).getMain().getTempMax() + "K";
                        textView.setText(s);
                        Toast.makeText(getApplicationContext(), "Weather updated successfully.",
                                Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Fetching Weather failed",
                            Toast.LENGTH_LONG).show();
                }

            }
        });

    }


}
